[![Go Reference](https://pkg.go.dev/badge/gitlab.com/gjuyn/go-bytepad/bytepad.svg)](https://pkg.go.dev/gitlab.com/gjuyn/go-bytepad/bytepad)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/gjuyn%2Fgo-config)](https://gitlab.com/gjuyn/go-bytepad/-/commits/master)
[![GitLab Release](https://img.shields.io/gitlab/v/release/gjuyn%2Fgo-config)](https://gitlab.com/gjuyn/go-bytepad/-/releases)
[![pipeline status](https://gitlab.com/gjuyn/go-bytepad/badges/master/pipeline.svg)](https://gitlab.com/gjuyn/go-bytepad/commits/master)
[![coverage report](https://gitlab.com/gjuyn/go-bytepad/badges/master/coverage.svg)](https://gitlab.com/gjuyn/go-bytepad/commits/master)

[[_TOC_]]

# Introduction

Simple light-weight zero-allocation byte pad.

Pad is a bytes buffer which incorporates a set of functions to add various standard go types to the buffer
as efficiently as possible.

Pad implements io.ReadWriteCloser, so it can easily be used in places where an io.Reader is expected.
E.g. as the body for http.NewRequest().

The io.Closer implementation automatically releases the Pad to the memory pool of its Factory.

The global Factory is used to instantiate a Pad with at least 128 bytes buffer size.
If you require larger initial sizes, you can easily instantiate your own Factory.
The global factory discards released pads if their capacity has grown over 64KB.

# Installation

```shell
go get gitlab.com/gjuyn/go-bytepad
```

```go
import "gitlab.com/gjuyn/go-bytepad"
```

# Pad

## Types

- `Pad`
  Pad encapsulates a bytes.Buffer.
  Do not instantiate a Pad directly.
  It is almost always better to use a Factory to instantiate a Pad, as it can easily be reused to relieve pressure on the GC. 
  A Pad is not safe for use across multiple go-routines.

## Functions

- `func NewPad(...int) *Pad`

  Instantiates a new Pad from the global factory.
  If the sum of the inputs is positive the capacity of the bytes buffer will be set to `max(128, sum)`.

- `func (*Pad) Release()`

  Returns the Pad to the memory pool of its factory.

- `func (*Pad) Reset()`

  Re-initializes the Pad to its initial state.

- `func (*Pad) Grow(...int)`

  Grows the capacity of the bytes buffer to at least the sum of the given sizes.
  If the capacity is already greater than the sum, the function does nothing.
  Any content already in the buffer is preserved.

- `func (*Pad) Bytes() []byte`

  Returns the current content of the bytes buffer.
  The returned slice is valid until the next `Reset()` / `Release()` call.

- `func (*Pad) String() string`

  Returns the current content of the bytes buffer as a string.
  Note that this will incur a memory allocation!

- `func (*Pad) Size() int`

  Returns the size of the current content in the bytes buffer.  

Most write functions return the Pad,  making it convenient to concatenate write calls.

- `func (*Pad) Write([]byte) (int, error)`

  Appends the input bytes to the current content of the bytes buffer.

- `func (*Pad) WriteLn() *Pad`

  Appends a linefeed (`'\n'`) to the current content of the bytes buffer.

- `func (*Pad) WriteByte(byte) error`

  Appends the input byte to the current content of the bytes buffer.

- `func (*Pad) WriteString(string) *Pad`

  Appends the input string to the current content of the bytes buffer.

- `func (*Pad) WriteRune(rune) *Pad`

  Appends the input rune to the current content of the bytes buffer.

- `func (*Pad) WriteBool(bool) *Pad`

  Appends the input boolean to the current content of the bytes buffer as the string `"true"` or `"false"`.

- `func (*Pad) WriteBoolInt(bool) *Pad`

  Appends the input boolean to the current content of the bytes buffer as the byte `'1'` or `'0'`.

- `func (*Pad) WriteInt64(int64, int) *Pad`

  Appends the input integer to the current content of the bytes buffer using `strconv.AppendInt()`.

- `func (*Pad) WriteUint64(uint64, int) *Pad`

  Appends the input unsigned integer to the current content of the bytes buffer using `strconv.AppendUint()`.

- `func (*Pad) WriteFloat64(float64, byte, int, int) *Pad`

  Appends the input number to the current content of the bytes buffer using `strconv.AppendFloat()`.

- `func (*Pad) WriteTime(time.Time, string) *Pad`

  Appends the date/time to the current content of the bytes buffer in the given layout using `time.AppendFormat()`.

- `func (*Pad) WriteDuration(time.Duration) *Pad`

  Appends the string representation of the input duration to the current content of the bytes buffer.
  This unfortunately endures a memory allocation. Consider writing the duration as an `int64` value!

I/O functionality:

- `func (*Pad) Flush(io.Writer) error`

  This function can be used to write the entire contents of the bytes buffer to the given writer.
  The returned error is the potential error returned by the 'Writer.Write()' implementation.
  The Pad will be reset to its initial state before the function returns, even if an error occurs.

- `func (*Pad) Read([]byte) (int, error)`

  Fill the input buffer as much as possible from the bytes buffer and return the length written.
  The returned error may be `io.EOF` if there are no more bytes in the buffer.
  If the size of the remaining bytes in the buffer is less than the size of the input buffer,
  all remaining bytes are written, and the returned length set to this size and the error to `io.EOF`. 

- `func (*Pad) Close() error`

  Close returns the Pad to the memory pool of its factory.
  Do not use the Pad after calling `Close()` as unexpected and potentially catastrophic things may happen.

## Constants

These all speak for themselves :)

- `TrueBytes = []byte("true")`
- `FalseBytes = []byte("false")`
- `TrueByte = byte('1')`
- `FalseByte = byte('0')`

# Pad factory

Factory represents the memory pool for Pads with a similar minimum/maximum buffer size.

A Pad acquired from a Factory is always instantiated with a minimum buffer size as defined for that Factory.
The maximum size is only used to discard a released Pad if its buffer has grown too large.

E.g. when a Pad is released it is only returned to the memory pool if `minSize` <= `cap(Pad)` <= `maxSize`.

## Types

- `Factory`

  The interface of a Pad factory.
  Use the `Acquire()` and `Release()` functions to instantiate a new Pad and return it to the memory pool.
  Factory is safe for use across multiple go-routines.

## Functions

- `Acquire(...int) *Pad`

  Instantiates a Pad from the global factory with a capacity of at least 128.
  If the sum of the inputs is positive, the capacity will be guaranteed to be at least `max(128, sum)`.
  Note that if the function returns a reused Pad from the memory pool it may already have a bigger capacity!

- `Release(*Pad)`

  Returns a used Pad to the global factory.


- `func NewFactory(opts ...FactoryOption)`

  Instantiates a new Pad factory with the given options.

- `func (*factory) Acquire(...int) *Pad`

  Instantiates a Pad from the memory pool with a capacity of at least the factories minimum buffer size.
  If the sum of the inputs is positive, the capacity will be guaranteed to be at least `max(f.minSize, sum)`.
  Note that if the function returns a reused Pad from the memory pool it may already have a bigger capacity!

- `func (*factory) Release(*Pad)`

  Returns a used Pad to the memory pool.
  The Pad is reset to its initial state first.
  If the Pad is marked as secret, the underlying buffer is overwritten with zeroes.

## Factory options

- `MinSize(int)`

  Use this option to set the minimum buffer size for new Pads.

- `MaxSize(int)`

  Use this option to set the maximum buffer size for Pads in the memory pool.
  If a released Pad has a greater buffer size, it will be discarded and returned to the GC.

- `MinMaxSize(int, int)`

  Convenience option to set the minimum and maximum buffer size in one call.

- `Secure()`

  Use this option to mark all Pads instantiated from this factory as secret.

# makefile

A `makefile` is included.
Run `make help` to find out more.

# Comments / bugs / merge requests

Feel free to use the usual means in this GitLab repository.

I have a busy life, so I can't guarantee anything, but I do try to reply to queries when I have the time.

# License

MIT.

Use at your own risk!
