package bytepad

import (
	"crypto/subtle"
)

// SecureClone makes a deep copy of the buffered bytes in Pad in constant time.
//
// This may be of use in a security sensitive context.
func SecureClone(p *Pad) []byte {
	buf := make([]byte, len(p.buf))
	subtle.ConstantTimeCopy(1, buf, p.buf)
	return buf
}

// SecureWrite appends the given input to the Pad in constant time.
//
// This may be of use in a security sensitive context.
//
// Be sure the capacity of the buffer is sufficient or this function may endure a memory allocation.
func SecureWrite(p *Pad, in []byte) {
	curLen, newLen, curCap := len(p.buf), len(in), cap(p.buf)
	needLen := curLen + newLen
	if needLen > curCap {
		p.Grow(needLen)
	}

	p.buf = p.buf[:needLen]
	subtle.ConstantTimeCopy(1, p.buf[curLen:], in)
}
