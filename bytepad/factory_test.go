package bytepad

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewBytesFactory(t *testing.T) {
	testCases := []struct {
		name    string
		minSize int
		maxSize int
		secure  bool
		minWant int
		maxWant int
	}{
		{name: "defaults", minWant: GlobalMinSize, maxWant: GlobalMaxSize},
		{name: "minSize 64", minSize: 64, minWant: 128, maxWant: GlobalMaxSize},
		{name: "maxSize 64", maxSize: 64, minWant: GlobalMinSize, maxWant: GlobalMinSize << 4},
		{name: "minSize 256", minSize: 256, minWant: 256, maxWant: GlobalMaxSize},
		{name: "maxSize 4096", maxSize: 4096, minWant: GlobalMinSize, maxWant: 4096},
		{name: "minSize 64. maxSize 4096", minSize: 64, maxSize: 4096, minWant: 128, maxWant: 4096},
		{name: "minSize 128. maxSize 64", minSize: 128, maxSize: 64, minWant: 128, maxWant: 2048},
		{name: "minSize 128. maxSize 128", minSize: 128, maxSize: 128, minWant: 128, maxWant: 128},
		{name: "minSize 256. maxSize 128", minSize: 256, maxSize: 128, minWant: 256, maxWant: 4096},
		{name: "minSize 256. maxSize 4096", minSize: 256, maxSize: 4096, minWant: 256, maxWant: 4096},
		{name: "secure", secure: true, minWant: GlobalMinSize, maxWant: GlobalMaxSize},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var opts []FactoryOption

			switch {
			case tc.minSize != 0 && tc.maxSize != 0:
				opts = append(opts, MinMaxSize(tc.minSize, tc.maxSize))
			case tc.minSize != 0:
				opts = append(opts, MinSize(tc.minSize))
			case tc.maxSize != 0:
				opts = append(opts, MaxSize(tc.maxSize))
			}

			if tc.secure {
				opts = append(opts, Secure())
			}

			f := NewFactory(opts...)
			require.NotNil(t, f)

			f2, ok := f.(*factory)
			require.True(t, ok)
			require.NotNil(t, f2)

			assert.Equal(t, tc.minWant, f2.minSize)
			assert.Equal(t, tc.maxWant, f2.maxSize)
			assert.Equal(t, tc.secure, f2.secret)
		})
	}
}

func TestAcquireRelease(t *testing.T) {
	testCases := []struct {
		name   string
		sizes  []int
		minCap int
	}{
		{name: "no sizes", minCap: 128},
		{name: "negative", sizes: []int{-1, 0}, minCap: 128},
		{name: "zero", sizes: []int{-1, 1, 0}, minCap: 128},
		{name: "positive", sizes: []int{0, 1, 2, 3}, minCap: 128},
		{name: "256", sizes: []int{64, 64, 64, 64}, minCap: 256},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			p := Acquire(tc.sizes...)
			require.NotNil(t, p)
			assert.GreaterOrEqual(t, tc.minCap, cap(p.buf))
			Release(p)
		})
	}
}

func TestRelease(t *testing.T) {
	testCases := []struct {
		name string
		in   *Pad
	}{
		{name: "nil"},
		{name: "no factory", in: &Pad{}},
		{name: "factory", in: Acquire(10, 20, 30, 40, 50)},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			Release(tc.in)
			Release(tc.in)
		})
	}
}
