package bytepad

import (
	"bytes"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPadStack(t *testing.T) {
	t.Run("pad (stack)", func(t *testing.T) {
		p := Pad{}
		assert.Zero(t, p.Size())

		s := "hello world"
		p.WriteString(s)
		require.NotNil(t, p.buf)
		assert.Equal(t, len(s), p.Size())

		p.Release()
	})
}

func TestNewPad(t *testing.T) {
	t.Run("new pad", func(t *testing.T) {
		p := NewPad(128, 128)
		require.NotNil(t, p)
		defer p.Release()

		require.NotNil(t, p.buf)
		assert.Zero(t, p.Size())
		assert.GreaterOrEqual(t, cap(p.buf), 256)

		p2 := NewPad()
		require.NotNil(t, p2)
		defer p2.Release()

		require.NotNil(t, p2.buf)
		assert.Zero(t, p2.Size())
		assert.GreaterOrEqual(t, cap(p2.buf), GlobalMinSize)
	})
}

func TestPadAcquire(t *testing.T) {
	t.Run("acquire pad", func(t *testing.T) {
		p := Acquire(128, 128)
		require.NotNil(t, p)
		defer p.Release()

		require.NotNil(t, p.buf)
		assert.Zero(t, p.Size())
		assert.GreaterOrEqual(t, cap(p.buf), 256)

		p2 := Acquire()
		require.NotNil(t, p2)
		defer p2.Release()

		require.NotNil(t, p2.buf)
		assert.Zero(t, p2.Size())
		assert.GreaterOrEqual(t, cap(p2.buf), GlobalMinSize)
	})
}

func TestPad_Reset(t *testing.T) {
	t.Run("secure reset", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.secret = true

		p.WriteStrings("Hello World!")
		assert.NotEmpty(t, p.buf)

		p.Reset()
		assert.Empty(t, p.buf)
		require.GreaterOrEqual(t, cap(p.buf), 256)

		b := p.buf[:cap(p.buf)]
		for i := range b {
			assert.Zero(t, b[i])
		}
	})
}

func TestBytes_Grow(t *testing.T) {
	t.Run("bytes grow", func(t *testing.T) {
		p := &Pad{buf: make([]byte, 0, 128)}
		p.WriteStrings("Hell", "o", " ", "Wo", "rld", "!")

		p.secret = true
		assert.Equal(t, "Hello World!", p.String())

		p.Grow()
		assert.Equal(t, 256, cap(p.buf))

		p.WriteString(" Was")
		_ = p.WriteByte('s')
		p.WriteString("up")

		assert.NotZero(t, p.Size())
		assert.Equal(t, "Hello World! Wassup", p.String())

		p.Reset()
		assert.GreaterOrEqual(t, cap(p.buf), 256)

		b := p.buf[:cap(p.buf)]
		for i := range b {
			assert.Zero(t, b[i])
		}

		p.Grow()
		assert.Equal(t, 512, cap(p.buf))

		p.Grow(100, 100, 100, 100, 100)
		assert.Equal(t, 512, cap(p.buf))
	})
}

func TestBytes_Write(t *testing.T) {
	t.Run("write", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		_, _ = p.Write([]byte("hello world"))
		assert.Equal(t, "hello world", string(p.Bytes()))
	})
}

func TestBytes_WriteBytes(t *testing.T) {
	t.Run("write byte", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteBytes([]byte{'a', 'b', '-', 'd', 'e'})
		assert.Equal(t, "ab-de", string(p.Bytes()))
	})
}

func TestBytes_WriteString(t *testing.T) {
	t.Run("write string", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteString("hello world")
		assert.Equal(t, "hello world", string(p.Bytes()))
	})
}

func TestBytes_WriteStrings(t *testing.T) {
	t.Run("write string", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteStrings("Hello", " ", "world", "! ", "How", " are ", "you?")
		assert.Equal(t, "Hello world! How are you?", p.String())
	})
}

func TestBytes_WriteRune(t *testing.T) {
	t.Run("write rune", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteRune('z').WriteRune('y').WriteRune('x').WriteRune('w').WriteRune('v')
		assert.Equal(t, "zyxwv", string(p.Bytes()))
	})
}

func TestBytes_WriteBool(t *testing.T) {
	t.Run("write bool", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteBool(true).WriteBool(false).WriteBool(true)
		assert.Equal(t, "truefalsetrue", string(p.Bytes()))
	})
}

func TestBytes_WriteBoolInt(t *testing.T) {
	t.Run("write bool int", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteBoolInt(true).WriteBoolInt(false).WriteBoolInt(true)
		assert.Equal(t, "101", string(p.Bytes()))
	})
}

func TestBytes_WriteInt64(t *testing.T) {
	t.Run("write int64", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteInt64(0, 10).WriteInt64(-9, 10).WriteInt64(-99, 10).WriteInt64(1<<16, 10)
		assert.Equal(t, "0-9-9965536", string(p.Bytes()))
	})
}

func TestBytes_WriteUint64(t *testing.T) {
	t.Run("write uint64", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteUint64(0, 10).WriteUint64(9, 10).WriteUint64(99, 10).WriteUint64(1<<16, 10)
		assert.Equal(t, "099965536", string(p.Bytes()))
	})
}

func TestBytes_WriteFloat64(t *testing.T) {
	t.Run("write float64", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteFloat64(0, 'g', -1, 64).WriteFloat64(-9.9, 'g', -1, 64)
		p.WriteFloat64(-99.99, 'g', -1, 64).WriteFloat64(1<<16, 'g', -1, 64)
		assert.Equal(t, "0-9.9-99.9965536", string(p.Bytes()))
	})
}

func TestBytes_WriteLn(t *testing.T) {
	t.Run("write line", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteString("line 1").WriteLn()
		p.WriteString("line 2").WriteLn()
		p.WriteString("line 3").WriteLn()
		p.WriteString("line 4").WriteLn()
		assert.Equal(t, "line 1\nline 2\nline 3\nline 4\n", string(p.Bytes()))
	})
}

func TestBytes_WriteTime(t *testing.T) {
	t.Run("write time", func(t *testing.T) {
		p := NewPad()
		require.NotNil(t, p)
		defer p.Release()

		d := time.Date(2023, 11, 25, 14, 19, 52, 123456789, time.UTC)

		p.WriteTime(d, time.RFC3339Nano)
		assert.Equal(t, "2023-11-25T14:19:52.123456789Z", p.String())
	})
}

func TestBytes_WriteDuration(t *testing.T) {
	t.Run("write duration", func(t *testing.T) {
		p := NewPad()
		require.NotNil(t, p)
		defer p.Release()

		d := 11*time.Hour + 23*time.Second + 511*time.Millisecond + 285*time.Microsecond

		p.WriteDuration(d)
		assert.Equal(t, "11h0m23.511285s", p.String())
	})
}

func TestBytes_Reset(t *testing.T) {
	t.Run("reset", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteString("hello world")
		assert.Equal(t, "hello world", string(p.Bytes()))

		p.Reset()
		assert.Zero(t, p.Size())

		p.WriteString("hello world")
		assert.Equal(t, "hello world", string(p.Bytes()))
	})
}

func TestBytes_Size(t *testing.T) {
	t.Run("size", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteString("hello world")
		assert.Equal(t, "hello world", string(p.Bytes()))
		assert.Equal(t, len("hello world"), p.Size())

		p.Reset()
		assert.Zero(t, p.Size())
	})
}

func TestBytes_Flush(t *testing.T) {
	t.Run("size", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)
		defer p.Release()

		p.WriteString("hello").WriteBytes([]byte{' '}).WriteString("world").WriteLn()

		b2 := bytes.Buffer{}
		err := p.Flush(&b2)
		require.NoError(t, err)
		assert.Equal(t, "hello world\n", string(b2.Bytes()))
		assert.Zero(t, p.Size())
	})
}

func TestBytes_ReadClose(t *testing.T) {
	t.Run("read/close", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)

		for ix := 0; ix < 10; ix++ {
			p.WriteString("hello world\n")
		}

		b2 := &bytes.Buffer{}
		n, err := b2.ReadFrom(p)
		require.NoError(t, err)
		assert.Equal(t, 10*len("hello world\n"), int(n))
		assert.Equal(t, int(n), b2.Len())

		err = p.Close()
		require.NoError(t, err)
	})
}

func TestBytes_DoubleClose(t *testing.T) {
	t.Run("double close", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)

		for ix := 0; ix < 10; ix++ {
			p.WriteString("hello world\n")
		}

		err := p.Close()
		require.NoError(t, err)

		err = p.Close()
		require.NoError(t, err)
	})
}

func TestBytes_ReadEOF(t *testing.T) {
	t.Run("read eof", func(t *testing.T) {
		p := NewPad(256)
		require.NotNil(t, p)

		for ix := 0; ix < 10; ix++ {
			p.WriteString("hello world\n")
		}

		b2 := make([]byte, 64)
		n, err := p.Read(b2)
		require.NoError(t, err)
		assert.Equal(t, 64, n)

		n, err = p.Read(b2)
		require.Error(t, err)
		assert.Equal(t, 56, n)

		n, err = p.Read(b2)
		require.Error(t, err)
		assert.Zero(t, n)

		err = p.Close()
		require.NoError(t, err)
	})
}

func BenchmarkBytes_Write(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		_, _ = p.Write([]byte("hello world!"))
	}
}

func BenchmarkBytes_WriteByte(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		_ = p.WriteByte('a')
	}
}

func BenchmarkBytes_WriteString(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteString("hello world!")
	}
}

func BenchmarkBytes_WriteInt_0(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteInt64(0, 10)
	}
}

func BenchmarkBytes_WriteInt_99(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteInt64(99, 10)
	}
}

func BenchmarkBytes_WriteInt_100(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteInt64(100, 10)
	}
}

func BenchmarkBytes_WriteInt_large(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	var n int64 = 1 << 62

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteInt64(n, 10)
	}
}

func BenchmarkBytes_WriteBool_True(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteBool(true)
	}
}

func BenchmarkBytes_WriteBool_False(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteBool(false)
	}
}

func BenchmarkBytes_WriteFloat_0(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteFloat64(0, 'g', -1, 64)
	}
}

func BenchmarkBytes_WriteFloat_99(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteFloat64(99, 'g', -1, 64)
	}
}

func BenchmarkBytes_WriteFloat_100(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteFloat64(100, 'g', -1, 64)
	}
}

func BenchmarkBytes_WriteFloat_large(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	var n float64 = 1 << 62

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteFloat64(n, 'g', -1, 64)
	}
}

func BenchmarkBytes_WriteRFC3339Nano(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	t := time.Now()

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteTime(t, time.RFC3339Nano)
	}
}

func BenchmarkBytes_WriteDuration_Small(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	d := 7 * time.Second

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteDuration(d)
	}
}

func BenchmarkBytes_WriteDuration_Large(b *testing.B) {
	p := NewPad(256)
	defer p.Release()

	d := time.Duration(1<<62 - 1)

	for i := 0; i < b.N; i++ {
		p.Reset()
		p.WriteDuration(d)
	}
}
