package bytepad

import (
	"crypto/rand"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSecureClone(t *testing.T) {
	t.Run("secure clone", func(t *testing.T) {
		p := NewPad()
		require.NotNil(t, p)
		defer p.Release()

		p.WriteString("He")
		p.WriteByte('l')
		p.WriteStrings("lo", " ", "World", "!")

		b := SecureClone(p)
		require.NotNil(t, b)
		assert.Equal(t, p.Size(), len(b))
		assert.EqualValues(t, p.Bytes(), b)
	})
}

func TestSecureWrite(t *testing.T) {
	t.Run("secure write", func(t *testing.T) {
		p := NewPad()
		require.NotNil(t, p)
		defer p.Release()

		s := 77
		b := make([]byte, s)
		l, err := rand.Read(b)
		require.NoError(t, err)
		require.Equal(t, len(b), l)

		for i := 0; i < 4; i++ {
			SecureWrite(p, b)
		}

		assert.Equal(t, 4*s, p.Size())
		assert.EqualValues(t, b, p.buf[0:s])
		assert.EqualValues(t, b, p.buf[s:2*s])
		assert.EqualValues(t, b, p.buf[2*s:3*s])
		assert.EqualValues(t, b, p.buf[3*s:4*s])
	})
}
