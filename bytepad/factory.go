package bytepad

import (
	"sync"
)

// Acquire retrieves a fresh Pad from the global factory.
func Acquire(sizes ...int) *Pad {
	return global.Acquire(sizes...)
}

// Release returns a used Pad to the global factory for recycling.
func Release(b *Pad) {
	global.Release(b)
}

// the global Pad factory.
var global = NewFactory()

// Factory is the interface for a Pad factory.
//
// A Factory is safe to use from concurrent go routines.
type Factory interface {
	Acquire(sizes ...int) *Pad
	Release(*Pad)
}

// NewFactory instantiates a new Pad factory with the given options.
//
// A freshly instantiated Pad from this factory is guaranteed to have a capacity of at least f.minSize.
func NewFactory(opts ...FactoryOption) Factory {
	f := &factory{minSize: GlobalMinSize, maxSize: GlobalMaxSize}
	for i := range opts {
		opts[i](f)
	}

	f.pool.New = func() any { return &Pad{buf: make([]byte, 0, f.minSize)} }
	return f
}

// Acquire retrieves a fresh Pad from the factory.
//
// If the sum of the input sizes is positive, the Pad is guaranteed to have a capacity of max(f.minSize, sum).
func (f *factory) Acquire(sizes ...int) *Pad {
	p := f.pool.Get().(*Pad)
	p.secret = f.secret
	p.readFrom = 0
	p.factory = f
	if len(sizes) > 0 {
		p.Grow(sizes...)
	}
	return p
}

// Release returns a used Pad to the factory for recycling.
//
// If the Pad has been marked as secure, the used bytes of the Pad will be overwritten with zeroes first.
func (f *factory) Release(p *Pad) {
	if p == nil || p.factory == nil {
		return
	}

	if p.secret {
		p.Reset()
	}
	p.factory = nil

	if cap(p.buf) >= f.minSize && cap(p.buf) <= f.maxSize {
		if !p.secret {
			p.Reset()
		}
		f.pool.Put(p)
	}
}

// FactoryOption is the option function signature for instantiating Factories.
type FactoryOption func(f *factory)

// MinSize defines the minimum size for Pads from the factory.
//
// The default is 128. A smaller size than 128 is not supported.
func MinSize(size int) FactoryOption {
	return func(f *factory) {
		if size > GlobalMinSize {
			f.minSize = size
		}
	}
}

// MaxSize defines the maximum size for Pads from the factory.
//
// The default is 65536.
// If the given size is less than the factory minimum size,
// the factory maximum size will be set to the minimum size * 16.
func MaxSize(size int) FactoryOption {
	return func(f *factory) {
		if size > 0 {
			f.maxSize = size
		}
		if f.maxSize < f.minSize {
			f.maxSize = f.minSize << 4
		}
	}
}

// MinMaxSize defines the minimum & maximum size for Pads from the factory.
//
// The defaults are 128 & 65536 respectively.
// If the given maximum size is less than the given minimum size,
// the factory maximum size will be set to the minimum size * 16.
func MinMaxSize(min, max int) FactoryOption {
	return func(f *factory) {
		if min > GlobalMinSize {
			f.minSize = min
		}
		if max > 0 {
			f.maxSize = max
		}
		if f.maxSize < f.minSize {
			f.maxSize = f.minSize << 4
		}
	}
}

// Secure makes any instantiated Pad from the factory secret by default.
//
// This ensures the underlying buffer content is wiped clean during a reset,
// and before the Pad is returned to the factory pool.
func Secure() FactoryOption {
	return func(f *factory) {
		f.secret = true
	}
}

type factory struct {
	secret  bool
	minSize int
	maxSize int
	pool    sync.Pool
}

// List of Global size constants.
const (
	GlobalMinSize = 1 << 7
	GlobalMaxSize = 1 << 16
)
