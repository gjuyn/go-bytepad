package bytepad

import (
	"io"
	"strconv"
	"time"
)

// Pad represents a light-weight reusable bytes buffer.
//
// Many convenient write functions are defined to stream standard Go types as efficiently as possible into the buffer.
//
// The write functions make use of standard byte-oriented conversion functions,
// so they can append the input to the buffer with zero memory allocations.
// The only exception here is WriteDuration() which unfortunately has to endure one small memory allocation.
// Consider writing durations as int64 instead.
//
// As most functions will compile inline,
// Pad can also be used as a wrapper around bytes.Buffer with extended write functions.
//
// Pad implements io.ReadWriteCloser for convenience.
// When io.Closer is called, the Pad is automatically released to its factory.
// This should be used with care, as the Pad must not be used after this call!
//
// Instead of instantiating Pads directly, please use a Factory to easily reuse Pads and relieve pressure on the GC.
//
// A Pad is not safe for use from concurrent go routines.
type Pad struct {
	secret   bool    // marks a 'secret' bytes buffer.
	readFrom int     // keep track of io.Reader position.
	factory  Factory // the Factory the Pad was instantiated from.
	buf      []byte  // the actual bytes buffer.
	io.ReadWriteCloser
}

// NewPad instantiates a new Pad with the given size from the global factory.
//
// If the sum of the sizes is positive, the buffer is guaranteed to have a capacity of at least this sum.
// The default and minimum capacity is 128 bytes.
func NewPad(sizes ...int) *Pad {
	p := Acquire()
	if len(sizes) > 0 {
		p.Grow(sizes...)
	}
	return p
}

// Release returns the Pad to the factory it was acquired from.
//
// If it was not acquired from a factory, it will be returned to the GC.
// Note that use of the Pad, after it is released to its factory, may cause catastrophic and hard to trace bugs!
func (p *Pad) Release() {
	if p.factory != nil {
		p.factory.Release(p)
	}
}

// Reset resets the Pad to its initial state.
func (p *Pad) Reset() {
	if p.secret {
		clear(p.buf)
	}
	p.buf = p.buf[:0]
	p.readFrom = 0
}

// Grow increases the capacity of the bytes buffer if the current capacity is less than the sum of the given sizes.
//
// As a special case, when no sizes are specified, the capacity of the bytes buffer is doubled.
//
// If the capacity grows, a new buffer is allocated, the old content is copied to the new buffer,
// and the Pad will point to the new buffer.
// If the Pad is secret, the old buffer will be overwritten with zeroes before it is discarded to the GC.
func (p *Pad) Grow(sizes ...int) {
	if size := p.sumSizes(sizes); cap(p.buf) < size {
		newBuf := make([]byte, len(p.buf), size)
		copy(newBuf, p.buf)
		if p.secret {
			clear(p.buf)
		}
		p.buf = newBuf
	}
}

func (p *Pad) sumSizes(sizes []int) int {
	if len(sizes) == 0 {
		return cap(p.buf) * 2
	}

	var size int
	for _, s := range sizes {
		size += s
	}
	return size
}

// Bytes returns the slice of buffered bytes.
//
// Note that the returned slice is only valid until the the Pad is reset/released!
func (p *Pad) Bytes() []byte {
	return p.buf
}

// String returns the slice of buffered bytes as a string.
//
// Note that this will make a deep copy into a new heap allocated string!
func (p *Pad) String() string {
	return string(p.buf)
}

// Size returns the number of buffered bytes.
func (p *Pad) Size() int {
	return len(p.buf)
}

// Write implements the io.Writer interface.
func (p *Pad) Write(in []byte) (int, error) {
	p.buf = append(p.buf, in...)
	return len(in), nil
}

// WriteBytes adds the input bytes to the buffer.
func (p *Pad) WriteBytes(in []byte) *Pad {
	p.buf = append(p.buf, in...)
	return p
}

// WriteLn adds a newline character to the buffer.
func (p *Pad) WriteLn() *Pad {
	p.buf = append(p.buf, '\n')
	return p
}

// WriteByte adds the input byte to the buffer.
//
// Note that this function returns `error`, and not `*Pad`.
func (p *Pad) WriteByte(in byte) error {
	p.buf = append(p.buf, in)
	return nil
}

// WriteString adds the input string to the buffer.
func (p *Pad) WriteString(in string) *Pad {
	p.buf = append(p.buf, in...)
	return p
}

// WriteStrings adds the input strings to the buffer.
func (p *Pad) WriteStrings(in ...string) *Pad {
	for ix := range in {
		p.buf = append(p.buf, in[ix]...)
	}
	return p
}

// WriteRune adds the input rune to the buffer.
func (p *Pad) WriteRune(in rune) *Pad {
	p.buf = append(p.buf, string(in)...)
	return p
}

// WriteBool adds the input boolean to the buffer as 'true' or 'false'.
func (p *Pad) WriteBool(in bool) *Pad {
	if in {
		return p.WriteBytes(TrueBytes)
	}
	return p.WriteBytes(FalseBytes)
}

// WriteBoolInt adds the input boolean to the buffer as '1' or '0'.
func (p *Pad) WriteBoolInt(in bool) *Pad {
	if in {
		p.buf = append(p.buf, TrueByte)
	} else {
		p.buf = append(p.buf, FalseByte)
	}
	return p
}

// WriteInt64 adds the input integer to the buffer.
func (p *Pad) WriteInt64(in int64, base int) *Pad {
	p.buf = strconv.AppendInt(p.buf, in, base)
	return p
}

// WriteUint64 adds the input integer to the buffer.
func (p *Pad) WriteUint64(in uint64, base int) *Pad {
	p.buf = strconv.AppendUint(p.buf, in, base)
	return p
}

// WriteFloat64 adds the input float to the buffer.
func (p *Pad) WriteFloat64(in float64, fmt byte, prec, bits int) *Pad {
	p.buf = strconv.AppendFloat(p.buf, in, fmt, prec, bits)
	return p
}

// WriteTime adds the input time to the buffer in the given layout.
func (p *Pad) WriteTime(in time.Time, layout string) *Pad {
	p.buf = in.AppendFormat(p.buf, layout)
	return p
}

// WriteDuration adds the input duration to the buffer in the given layout.
//
// Note that this function unfortunately endures a memory allocation!
func (p *Pad) WriteDuration(in time.Duration) *Pad {
	p.buf = append(p.buf, in.String()...)
	return p
}

// Flush writes the buffered bytes to the given writer, and resets the Pad afterwards.
//
// The function returns any error the writer may return from the write operation.
func (p *Pad) Flush(w io.Writer) error {
	_, err := w.Write(p.buf)
	p.Reset()
	return err
}

// Read implements the io.Reader interface.
func (p *Pad) Read(out []byte) (n int, err error) {
	if p.readFrom >= len(p.buf) {
		return 0, io.EOF
	}

	l := copy(out, p.buf[p.readFrom:])
	p.readFrom += l

	if p.readFrom >= len(p.buf) {
		return l, io.EOF
	}
	return l, nil
}

// Close implements the io.Closer interface.
//
// It will automatically release the Pad to the factory it was acquired from.
// Note that use of the Pad after this function may likely cause catastrophic and hard to trace errors!
func (p *Pad) Close() error {
	p.Release()
	return nil
}

// useful constants.
var (
	TrueBytes       = []byte("true")
	FalseBytes      = []byte("false")
	TrueByte   byte = '1'
	FalseByte  byte = '0'
)
